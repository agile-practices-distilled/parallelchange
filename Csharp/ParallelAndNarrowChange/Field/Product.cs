namespace ParallelAndNarrowChange.Field
{
    class Product
    {
        private decimal _price;

        public decimal Price => _price;

        public Product(decimal price)
        {
            _price = price;
        }
    }
}