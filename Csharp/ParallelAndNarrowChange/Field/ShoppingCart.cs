﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace ParallelAndNarrowChange.Field
{
    public class ShoppingCart{
        private readonly List<Product> _products = new List<Product>();
        private const int EXPENSIVE_PRODUCT_THRESHOLD = 100;

        public decimal CalculateTotalPrice(){
            return _products.Sum(p => p.Price);
        }

        public bool HasDiscount()
        {
            return _products.Any(p => p.Price > EXPENSIVE_PRODUCT_THRESHOLD);
        }

        public void Add(int aPrice){
            _products.Add(new Product(aPrice));
        }

        public int NumberOfProducts(){
            return _products.Count;
        }
    }
}
